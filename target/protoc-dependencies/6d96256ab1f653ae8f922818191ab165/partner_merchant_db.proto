syntax = "proto3";

option java_package = "co.ritual.backend.partner.merchant.data";

package co.ritual.backend.partner.merchant.data;

// proto for partners.merchant_fee_tier table
message MerchantFeeTierDb {

    // column: merchant_fee_tier_id type: varchar(32)
    string merchant_fee_tier_id = 1;

    // column: merchant_terms_id type: varchar(32)
    string merchant_terms_id = 2;

    // column: merchant_id type: varchar(32)
    string merchant_id = 3;

    // column: tier_type type: enum('REGULAR','BIGBANG_V3','TRIAL')
    enum TierType {
        REGULAR = 0;
        BIGBANG_V3 = 1;
        TRIAL = 2;
    }

    TierType tier_type = 4;

    // column: is_override_tier type: tinyint(1)
    bool is_override_tier = 5;

    // column: corporate_discount_percent type: double
    double corporate_discount_percent = 6;

    // column: transaction_fee_percent type: double
    double transaction_fee_percent = 7;

    // column: transaction_fee_dollar type: double
    double transaction_fee_dollar = 8;

    // column: tier_start_at_micro type: bigint(20)
    int64 tier_start_at_micro = 9;

    // column: is_current type: tinyint(4)
    bool is_current = 10;

    // column: tip_fee_percent type: double
    double tip_fee_percent = 11;

    // column: fee_structure_start_date type: datetime(3)
    int64 fee_structure_start_date = 12;

    // column: fee_structure_end_date type: datetime(3)
    int64 fee_structure_end_date = 13;

    // column: created_at type: datetime(3)
    int64 created_at = 14;

    // column: updated_at type: datetime(3)
    int64 updated_at = 15;

}

// proto for partners.merchant_terms table
message MerchantTermsDb {

    // column: merchant_terms_id type: varchar(32)
    string merchant_terms_id = 1;

    // column: merchant_id type: varchar(32)
    string merchant_id = 2;

    // column: terms_sheet_id type: varchar(32)
    string terms_sheet_id = 3;

    // column: signed_user_id type: varchar(32)
    string signed_user_id = 4;

    // column: signed_datetime type: datetime(3)
    int64 signed_datetime = 5;

    // column: created_at type: datetime(3)
    int64 created_at = 6;

    // column: updated_at type: datetime(3)
    int64 updated_at = 7;

}

// proto for partners.merchant_agreement table
message MerchantAgreementDb {

    // column: merchant_agreement_id type: int(11)
    int32 merchant_agreement_id = 1;

    // column: merchant_id type: varchar(32)
    string merchant_id = 2;

    // column: agreement_sheet_id type: varchar(32)
    string agreement_sheet_id = 3;

    // column: signed_user_id type: varchar(32)
    string signed_user_id = 4;

    // column: signed_ip_address type: varchar(32)
    string signed_ip_address = 5;

    // column: signed_datetime type: datetime(3)
    int64 signed_datetime = 6;

    // column: created_at type: datetime(3)
    int64 created_at = 7;

    // column: updated_at type: datetime(3)
    int64 updated_at = 8;

}

// proto for partners.merchant_contact_info table
message MerchantContactInfoDb {

    // column: contact_id type: varchar(32)
    string contact_id = 1;

    // column: merchant_id type: varchar(32)
    string merchant_id = 2;

    // column: first_name type: varchar(45)
    string first_name = 3;

    // column: last_name type: varchar(45)
    string last_name = 4;

    // column: email type: varchar(255)
    string email = 5;

    // column: phone_number type: varchar(16)
    string phone_number = 6;

    // column: phone_country_code type: varchar(2)
    string phone_country_code = 7;

    // column: phone_calling_code type: varchar(4)
    string phone_calling_code = 8;

    // column: is_primary type: tinyint(1)
    bool is_primary = 9;

    // column: created_at type: datetime(3)
    int64 created_at = 10;

    // column: updated_at type: datetime(3)
    int64 updated_at = 11;
}

// proto for partners.merchant_onboarding_info table
message MerchantOnboardingInfoDb {

    // column: merchant_id type: varchar(32)
    string merchant_id = 1;

    // column: general_terms_datetime type: datetime(3)
    int64 general_terms_datetime = 2;

    // column: merchant_agreement_datetime type: datetime(3)
    int64 merchant_agreement_datetime = 3;

    // column: contact_info_datetime type: datetime(3)
    int64 contact_info_datetime = 4;

    // column: banking_info_datetime type: datetime(3)
    int64 banking_info_datetime = 5;

    // column: confirmation_datetime type: datetime(3)
    int64 confirmation_datetime = 6;

    // column: is_opt_out_merchant_agreement type: tinyint(1)
    bool is_opt_out_merchant_agreement = 7;
}

// proto for partners.merchant_banking_info table
message MerchantBankingInfoDb {

    // column: merchant_id type: varchar(32)
    string merchant_id = 1;

    // column: legal_holder_name type: varchar(255)
    string legal_holder_name = 2;

    // column: bank_type type: enum('US','CA','AU','GB')
    enum BankType {
        US = 0;
        CA = 1;
        AU = 2;
        GB = 3;
    }

    BankType bank_type = 3;

    // column: account_details_json type: text
    string account_details_json = 4;

    // column: created_at type: datetime(3)
    int64 created_at = 5;

    // column: updated_at type: datetime(3)
    int64 updated_at = 6;
}

// proto for partners.merchant_form table
message MerchantFormDb {

    // column: form_type type: varchar(32)
    string form_type = 1;

    // column: form_description type: varchar(45)
    string form_description = 2;

    // column: is_deprecated type: tinyint(1)
    bool is_deprecated = 3;

    // column: created_at type: datetime(3)
    int64 created_at = 4;

    // column: updated_at type: datetime(3)
    int64 updated_at = 5;

}

// proto for partners.merchant_form_content table
message MerchantFormContentDb {

    // column: form_data_id type: varchar(32)
    string form_data_id = 1;

    // column: merchant_id type: varchar(32)
    string merchant_id = 2;

    // column: user_id type: varchar(32)
    string user_id = 3;

    // column: form_type type: varchar(32)
    string form_type = 4;

    // column: form_content_json type: text
    string form_content_json = 5;

    // column: created_at type: datetime(3)
    int64 created_at = 6;

    // column: updated_at type: datetime(3)
    int64 updated_at = 7;

}

// proto for partners.merchant_form_geo_association table
message MerchantFormGeoAssociationDb {

    // Primary key for MerchantFormGeoAssociation
    message Key {
        // column: form_type type: varchar(32)
        string form_type = 1;
        // column: country_code type: varchar(64)
        string country_code = 2;
        // column: region_code type: varchar(64)
        string region = 3;
        // column: metro_id type: varchar(64)
        string metro_id = 4;
    }

    Key key = 1;

    // column: form_type type: varchar(32)
    string form_type = 2;

    // column: country_code type: varchar(64)
    string country_code = 3;

    // column: region type: varchar(64)
    string region = 4;

    // column: metro_id type: varchar(64)
    string metro_id = 5;

    // column: is_optional type: tinyint(1)
    bool is_optional = 6;

    // column: created_at type: datetime(3)
    int64 created_at = 7;

    // column: updated_at type: datetime(3)
    int64 updated_at = 8;

}

// proto for stingray.partner_payment_info table
message PartnerPaymentInfoDb {

    // column: merchant_id type: varchar(32)
    string merchant_id = 1;

    // column: hst_number type: varchar(20)
    string hst_number = 2;

    // column: payment_type type: enum('UNKNOWN_PAYMENT_TYPE','CREDIT_CARD','CHEQUE','GIFT_CARD','DIRECT_DEPOSIT')
    enum PaymentType {
        UNKNOWN_PAYMENT_TYPE = 0;
        CREDIT_CARD = 1;
        CHEQUE = 2;
        GIFT_CARD = 3;
        DIRECT_DEPOSIT = 4;
    }

    PaymentType payment_type = 3;

    // column: payment_window type: enum('UNKNOWN_PAYMENT_WINDOW','ON_DEMAND','PREPAYMENT','POSTPAYMENT')
    enum PaymentWindow {
        UNKNOWN_PAYMENT_WINDOW = 0;
        ON_DEMAND = 1;
        PREPAYMENT = 2;
        POSTPAYMENT = 3;
    }

    PaymentWindow payment_window = 4;

    // column: direct_deposit_bank type: varchar(32)
    string direct_deposit_bank = 6;

    // column: direct_deposit_customer type: int(11)
    int32 direct_deposit_customer = 7;

    // column: use_direct_deposit type: tinyint(4)
    bool use_direct_deposit = 8;

    // column: confirmed_payment_details type: tinyint(1)
    bool confirmed_payment_details = 9;

    // column: billing_address type: varchar(255)
    string billing_address = 15;

}

message PartnerBalanceDb {

    // column: merchant_id type: varchar(32)
    string merchant_id = 1;

    // column: balance_micro type: bigint(20)
    int64 balance_micro = 2;

    // column: update_datetime type: datetime(6)
    int64 update_datetime = 3;

}

// proto for stingray.partner_payment table
message PartnerPaymentDb {

    // column: merchant_id type: varchar(32)
    string merchant_id = 1;

    // column: pay_period_from type: date
    int64 pay_period_from = 2;

    // column: pay_period_to type: date
    int64 pay_period_to = 3;

    // column: amount_micro type: bigint(20)
    int64 amount_micro = 4;

    // column: create_datetime type: datetime(3)
    int64 create_datetime = 5;

    // primary key for partner_payment
    message Key {

        string merchant_id = 1;

        int64 pay_period_from = 2;

        int64 pay_period_to = 3;

    }
}