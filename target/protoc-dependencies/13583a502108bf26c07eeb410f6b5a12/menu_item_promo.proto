syntax = "proto3";

option java_package = "co.ritual.backend.proto.menu";

package menu.promo;

// proto for stingray.menu_item_promo table
message MenuItemPromoDb {

    // The menu item promo id.
    // column: menu_item_promo_id type: int(11)
    string menu_item_promo_id = 1;

    // The datetime representing the first day of the promo.
    // column: start_datetime type: datetime
    int64 start_datetime = 2;

    // The datetime representing the last day of the promo. If not set, then it means the promo has no end date.
    // column: end_datetime type: datetime
    int64 end_datetime = 3;

    // The rule that dictates who can see and use the perk item.
    //ALL_USERS: Available to all users. 1 only.
    //ALL_USERS_UNLIMITED: Available to all users. Unlimited.
    //NEW_TO_MERCHANT: Available only to users who haven't made a purchase at the merchant.
    //MERCHANT_CUSTOM: Available to users that haven't made a purchase at any of the participating merchants.
    //ALL_USERS_CUSTOM: Available to all users for a limited time. 1 only.
    //ALL_USERS_UNLIMITED_CUSTOM: Available to all Ritual users for a limited time. Unlimited.
    //BIG_BANG_FIRST_PURCHASE:Available to new users with no big bang id, or the merchant's big bang id. 1 only.
    //BIG_BANG_FIRST_PURCHASE_CUSTOM:Available to new users with no big bang id, or the merchant's big bang id for a limited time. 1 only.
    //ALL_USERS_APPLE_PAY_CUSTOM:Available to all users that pay with Apple Pay for a limited time. 1 only.
    //ALL_USERS_ANDROID_PAY_CUSTOM:Same as above for Android Pay.
    //ALL_USERS_ONCE_PER_DAY_CUSTOM:Available to all users 1x/day/user.
    //NEW_TO_MERCHANT_CUSTOM:Available to new users for a
    // column: user_eligibility type: enum('ALL_USERS','NEW_TO_MERCHANT','MERCHANT_CUSTOM','ALL_USERS_CUSTOM','ALL_USERS_UNLIMITED','ALL_USERS_UNLIMITED_CUSTOM','BIG_BANG_FIRST_PURCHASE','BIG_BANG_FIRST_PURCHASE_CUSTOM','ALL_USERS_APPLE_PAY_CUSTOM','ALL_USERS_ANDROID_PAY_CUSTOM','ALL_USERS_ONCE_PER_DAY_CUSTOM','NEW_TO_MERCHANT_CUSTOM','COMPANY_LOCATION_LUNCH_CLUB','NEW_TO_MERCHANT_ANDROID_PAY_CUSTOM','NEW_TO_MERCHANT_APPLE_PAY_CUSTOM','ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_CUSTOM','ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_APPLE_PAY_CUSTOM_MAX_FIVE', 'ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_CUSTOM_MAX_FIVE')
    enum UserEligibility {
        ALL_USERS = 0;
        NEW_TO_MERCHANT = 1;
        MERCHANT_CUSTOM = 2;
        ALL_USERS_CUSTOM = 3;
        ALL_USERS_UNLIMITED = 4;
        ALL_USERS_UNLIMITED_CUSTOM = 5;
        BIG_BANG_FIRST_PURCHASE = 6;
        BIG_BANG_FIRST_PURCHASE_CUSTOM = 7;
        ALL_USERS_APPLE_PAY_CUSTOM = 8;
        ALL_USERS_ANDROID_PAY_CUSTOM = 9;
        ALL_USERS_ONCE_PER_DAY_CUSTOM = 10;
        NEW_TO_MERCHANT_CUSTOM = 11;
        COMPANY_LOCATION_LUNCH_CLUB = 12;
        NEW_TO_MERCHANT_ANDROID_PAY_CUSTOM = 13;
        NEW_TO_MERCHANT_APPLE_PAY_CUSTOM = 14;
        ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_CUSTOM = 15;
        ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_CUSTOM_MAX_FIVE = 16;
        ALL_USERS_ONCE_PER_DAY_CUSTOM_MAX_FIVE = 17;
        ALL_USERS_ONCE_PER_DAY_ONCE_PER_MERCHANT_APPLE_PAY_CUSTOM_MAX_FIVE = 18;
    }

    UserEligibility user_eligibility = 4;

    // The limit on how many perks you can purchase in a single order.
    // column: limit_per_order type: int(11)
    int32 limit_per_order = 5;

    // Toggle for whether or not use must be signed in to see the perk. I.e. if set to true, web client users who aren't signed in can't see the perk.
    // column: is_sign_in_required type: tinyint(1)
    bool is_sign_in_required = 6;

    // Details on the perk terms.
    // column: fine_print type: varchar(45)
    string fine_print = 7;

    // column: internal_description type: varchar(128)
    string internal_description = 8;

    // Message displayed on top of the client's menu screen.
    // column: highlight_message type: varchar(255)
    string highlight_message = 9;

    // column: requisite_achievment_id type: varchar(64)
    string requisite_achievment_id = 10;

    // column: exclusive_achievement_id type: varchar(64)
    string exclusive_achievement_id = 11;

    // column: eligibility_rank type: int(11)
    int32 eligibility_rank = 12;

    // column: minimum_signup_timestamp type: bigint(20)
    int64 minimum_signup_timestamp = 13;

    // column: post_signup_eligibility_duration_days type: int(11)
    int32 post_signup_eligibility_duration_days = 14;

    UserSegment user_segment = 15;

    // column: user_not_activated type: tinyint(1)
    bool user_not_activated = 16;

    // column: user_has_big_bang_id type: tinyint(1)
    bool user_has_big_bang_id = 17;

    // column: is_signup_promo_gated type: tinyint(1)
    bool is_signup_promo_gated = 18;

    // column: payment_method_apple_pay type: tinyint(1)
    bool payment_method_apple_pay = 19;

    // column: payment_method_google_pay type: tinyint(1)
    bool payment_method_google_pay = 20;

    // column: redemptions_max type: int(11)
    int32 redemptions_max = 21;

    // column: redemptions_max_daily type: int(11)
    int32 redemptions_max_daily = 22;

    // column: redemptions_max_per_merchant type: int(11)
    int32 redemptions_max_per_merchant = 23;

    // This restricts perks by order type. Use one of ALL_ORDERS, SOLO, HOST, GUEST, PIGGYBACK
    // column: order_type_eligibility type: varchar(32)
    string order_type_eligibility = 24;

    // column: requisite_achievment_id type: varchar(45)
    string fine_print_template_id = 25;

    // column: requisite_achievment_id type: varchar(45)
    string highlight_message_template_id = 26;
}

// column: user_segment type: enum('ALL_USERS','NEW_TO_MERCHANT')
enum UserSegment {
  ALL_USERS = 0;
  NEW_TO_MERCHANT = 1;
}