syntax = "proto3";

package tax;

option java_package = "co.ritual.proto.data";

import "rounding.proto";

// A tax rate is a % rate + fixed amount in micro.
// For most taxes, it will probably be either 1 or the other.
// This design allows the flexibility through a single message.
message TaxRate {
  double percent_rate = 1;
  int64 fixed_amount_micro = 2;
  math.RoundingMode rounding_mode = 3;
}

// A tiered tax rate applies different TaxRate message when taxable amount
// exceeds that tier's tier_start_micro. Only the highest most surpassed tier's
// TaxRate will be used for calculation.
//
// Example:
// We have tier starts at $0 for 5% and a second tier at $4 for 13%
// When calculating tax for $3, we will use 5%
// When calculating tax for $4 or higher, we will use 13% (NOT 13% + 5%).
message TieredTaxRate {
  message Tier {
    int64 tier_start_micro = 1;
    TaxRate tax_rate = 2;
  }
  repeated Tier tier = 1;
}

// Configuration for a TaxRate evaluator. This message will likely be used
// as part of TaxRuleConfig and kept in persistent storage.
message TaxRateConfig {
  string tax_rate_id = 1;
  oneof config_one_of {
    TaxRate simple_tax_rate = 2;
    TieredTaxRate tiered_tax_rate = 3;
  }
}

// Configuration for a TaxRule. This is used to construct a TaxRule object.
message TaxRuleConfig {
  string tax_rule_id = 1;
  repeated TaxRateConfig tax_rate_config = 2;
}

// Result of applying a TaxRateEvaluator.
message TaxRateResult {
  string tax_rate_id = 1;
  TaxRate applied_tax_rate = 2;
  int64 taxable_micro = 3;
  int64 tax_micro = 4;
}

// Result of applying a TaxRule.
message TaxRuleResult {
  // The rule ID that generated this result.
  string tax_rule_id = 1;

  // Tax Rate ID to tax in micros.
  repeated TaxRateResult tax_rate_result = 2;

  // Total tax micro.
  int64 total_tax_micro = 4;
}

// Result of the user/merchant tax information on the order
message OrderTaxRuleResult {
  repeated TaxRuleResult tax_rule_result = 1;
}

