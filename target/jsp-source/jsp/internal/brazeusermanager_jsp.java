package jsp.internal;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class brazeusermanager_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\n  <head>\n    <title>Manage Braze User</title>\n    <style>\n      .client-payload {\n        background: #aaa;\n      }\n      .v1-notifications {\n        background: #aaa;\n      }\n      .notification-body {\n        background: #eee;\n      }\n      .group-order {\n        background: #aaa;\n      }\n      .cache-title {\n        font-size: 22px;\n        font-weight: bold;\n        text-align: center;\n      }\n    </style>\n  </head>\n  <body>\n      <hr>\n      <div class=\"page-body\">\n\n        <form action=\"/internal/brazeusermanager\" method=\"POST\">\n            <b>Modify or create user attributes in braze</b>\n            <div class=\"communication-body\">\n                <b>User ID</b><br>\n                <input type=\"text\" placeholder=\"External ID to send to\" name=\"external_id\" style=\"width: 500px; height: 20px;\"></input><br>\n\n                <b>Placeholders</b><br>\n                <input placeholder=\"key:value,key:value...\" name=\"placeholders\" type=\"text\"></input><br>\n                <br>\n\n                <b>Environment</b><br>\n");
      out.write("                <input name=\"environment\" value=\"CONSUMER\" type=\"radio\" checked=\"checked\"></input>Consumer<br>\n                <input name=\"environment\" value=\"REFERRAL\" type=\"radio\"></input>Referral<br>\n                <input name=\"environment\" value=\"PARTNER\" type=\"radio\"></input>Partner<br>\n\n                <br>\n\n                <input type=\"submit\" value=\"SEND\"/> <span style=\"color:red;\"></span>\n            </div>\n        </form>\n      </div>\n  </body>\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
