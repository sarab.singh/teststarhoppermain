package jsp.internal;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class sendcommunication_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\n  <head>\n    <title>Send Message</title>\n    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n    <style>\n      .client-payload {\n        background: #aaa;\n      }\n      .v1-notifications {\n        background: #aaa;\n      }\n      .notification-body {\n        background: #eee;\n      }\n      .group-order {\n        background: #aaa;\n      }\n      .cache-title {\n        font-size: 22px;\n        font-weight: bold;\n        text-align: center;\n      }\n    </style>\n  </head>\n  <body>\n      <div class=\"container-fluid\">\n          <form id=\"form\" action=\"/internal/sendcommunication\" method=\"POST\">\n              <!-- Header -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                  <h2>Braze Communication Test Tool</h2>\n                  <h6>Sends a test comm to an employee email address. Each individual comm may be sent at a rate of 1 per minute.</h6>\n");
      out.write("              </div>\n\n              <!-- User ID -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                <label for=\"external-id-input\" ><b>Email Address</b> *</label>\n                <div class=\"input-group\">\n                  <input class=\"form-control\" id=\"external-id-input\" name=\"email-address\" placeholder=\"Internal email address to send to\" required></input>\n                    <div class=\"input-group-append\">\n                        <span class=\"input-group-text\" id=\"basic-addon2\">@ritual.co</span>\n                    </div>\n                  </div>\n              </div>\n\n              <!-- Comm ID -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                <label for=\"template-name-input\" ><b>Braze Template Name</b> *</label>\n                <input class=\"form-control mdb-autocomplete\" id=\"template-name-input\" name=\"template-name\" required></input>\n                <small class=\"form-text text-muted\">This must match with an entry in CommunicationId.java, or with a Braze campaign ID.</small>\n");
      out.write("              </div>\n\n              <!-- Radio Buttons -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                <div class=\"form-row\">\n                  <!-- Braze Environment -->\n                  <div class=\"col-sm-6 mb-3\">\n                    <b>Braze Environment</b>\n                    <div class=\"custom-control custom-radio\">\n                      <input type=\"radio\" id=\"environment-radio-1\" name=\"environment-radio\" value=\"RITUAL_USERS\" class=\"custom-control-input\" checked=\"checked\">\n                      <label class=\"custom-control-label\" for=\"environment-radio-1\">Consumer</label>\n                    </div>\n                    <div class=\"custom-control custom-radio\">\n                      <input type=\"radio\" id=\"environment-radio-3\" name=\"environment-radio\" value=\"EXTERNAL\" class=\"custom-control-input\">\n                      <label class=\"custom-control-label\" for=\"environment-radio-3\">External</label>\n                    </div>\n                    <div class=\"custom-control custom-radio\">\n");
      out.write("                      <input type=\"radio\" id=\"environment-radio-2\" name=\"environment-radio\" value=\"PARTNERS\" class=\"custom-control-input\" disabled>\n                      <label class=\"custom-control-label\" for=\"environment-radio-2\">Partner</label>\n                    </div>\n                    <div class=\"custom-control custom-radio\">\n                      <input type=\"radio\" id=\"environment-radio-\" name=\"environment-radio\" value=\"CANVAS\" class=\"custom-control-input\">\n                      <label class=\"custom-control-label\" for=\"environment-radio-4\">Canvas</label>\n                    </div>\n                  </div>\n\n                  <!-- Comm Type -->\n                  <div class=\"col-sm-6 mb-3\">\n                      <b>Communication Type</b>\n                      <div class=\"custom-control custom-radio\">\n                        <input type=\"radio\" id=\"comm-type-radio-1\" name=\"comm-type-radio\" value=\"EMAIL\" class=\"custom-control-input\" checked=\"checked\">\n                        <label class=\"custom-control-label\" for=\"comm-type-radio-1\">Email</label>\n");
      out.write("                      </div>\n                      <div class=\"custom-control custom-radio\">\n                        <input type=\"radio\" id=\"comm-type-radio-2\" name=\"comm-type-radio\" value=\"NOTIFICATION\" class=\"custom-control-input\" disabled>\n                        <label class=\"custom-control-label\" for=\"comm-type-radio-2\">Notification</label>\n                      </div>\n                      <div class=\"custom-control custom-radio\">\n                        <input type=\"radio\" id=\"comm-type-radio-3\" name=\"comm-type-radio\" value=\"SMS\" class=\"custom-control-input\" disabled>\n                        <label class=\"custom-control-label\" for=\"comm-type-radio-3\">SMS</label>\n                      </div>\n                    </div>\n                </div>\n              </div>\n              \n              <!-- Placeholders -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                <label for=\"placeholders-input\" ><b>Template Placeholders</b></label>\n                <textarea rows=\"3\" class=\"form-control\" name=\"template-placeholders\" placeholder=\"key:value, key:value, ... or {key=value, key=value, ... }\"></textarea>\n");
      out.write("              </div>\n\n              <!-- Submit -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                  <button id=\"send-button\" class=\"btn btn-primary\" type=\"submit\">Send Comm</button>\n              </div>\n\n              <!-- Alerts -->\n              <div class=\"col-md-8 col-lg-6 col-xl-4 mb-3\">\n                  <div id=\"success-box\" class=\"alert alert-success\" style=\"display:none\">\n                      <strong>Success!</strong> <p style=\"display: inline;\">The email will be sent shortly.</p>\n                  </div>\n                  <div id=\"error-box\" class=\"alert alert-danger\" style=\"display:none\">\n                    <strong>Error!</strong> <p id=\"error-text\" style=\"display: inline;\"></p>\n                  </div>\n              </div>\n      </div>\n\n      <!-- Bootstrap Dependencies -->\n      <script src=\"https://code.jquery.com/jquery-3.4.1.min.js\"></script>\n      <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\" integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\" crossorigin=\"anonymous\"></script>\n");
      out.write("      <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\" integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\" crossorigin=\"anonymous\"></script>\n\n      <!-- Custom Javascript -->\n      <script type=\"text/javascript\" src=\"../js/sendcommunication.js\"></script>\n  </body>\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
