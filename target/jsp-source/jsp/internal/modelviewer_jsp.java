package jsp.internal;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import co.ritual.internal.modelviewer.ModelViewer;

public final class modelviewer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;


  ModelViewer modelViewer = (ModelViewer) request.getAttribute(ModelViewer.PARAM_MODEL_VIEWER);

      out.write("<html lang=\"en\">\n<head>\n  <meta charset=\"UTF-8\">\n  <title>JQuery JSONView</title>\n  <link rel=\"stylesheet\" href=\"/internal/responseviewer/jquery.jsonview.min.css\" />\n  <style>\n    body {\n      background-color: light-gray;\n      font-family: monospace;\n    }\n  </style>\n  <script type=\"text/javascript\" src=\"/internal/responseviewer/jquery.min.js\"></script>\n  <script type=\"text/javascript\" src=\"/internal/responseviewer/jquery.jsonview.min.js\"></script>\n  <script type=\"text/javascript\">\n    var json = ");
      out.print( modelViewer.getJson() );
      out.write(";\n    $(function() {\n      $(\"#json\").JSONView(json);\n      // $(\"#json\").JSONView(json, { collapsed: true, nl2br: true, recursive_collapser: true });\n      $('#toggle-btn').on('click', function() {\n        $('#json').JSONView('toggle');\n      });\n    });\n  </script>\n</head>\n<body>\n  <h2>Message</h2>\n  <div id=\"message\">");
      out.print( modelViewer.getMessage() );
      out.write("</div>\n  <h2>Data</h2>\n  <button id=\"toggle-btn\">Toggle</button>\n  <div id=\"json\"></div>\n</body>\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
