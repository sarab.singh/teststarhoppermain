package jsp.internal;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import co.ritual.internal.reloadcache.ReloadType;

public final class reloadcache_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\n  <head>\n    <title>Reload Cache</title>\n    <style>\n      .cache-container {\n        background: #eee;\n      }\n      .cache-title {\n        font-size: 22px;\n        font-weight: bold;\n        text-align: center;\n      }\n    </style>\n    <script>\n      function verifySubmit(target, key) {\n        if (key == 'admin') {\n          return true;\n        }\n\n        var targets = target.getElementsByTagName('textarea');\n        var hasId = false;\n        for (var i = 0; i < targets.length; i++) {\n          if (targets[i].value) {\n            hasId = true;\n            break;\n          }\n        }\n        document.getElementById(key + '_error').innerHTML = hasId ? '' : 'Please specify an id';\n        return hasId;\n      };\n      function onSelectCache(target) {\n        if (target.previous_value) {\n          document.getElementById(target.previous_value + '_panel').style.display = 'none';\n        }\n        document.getElementById(target.value + '_panel').style.display = 'block';\n        target.previous_value = target.value;\n");
      out.write("      }\n    </script>\n  </head>\n  <body onload=\"onSelectCache(document.getElementById('caches'));\">\n    <select id=\"caches\" onchange=\"onSelectCache(this);\">\n    ");
 for (ReloadType reloadType : ReloadType.values()) { 
      out.write("<option value=\"");
      out.print( reloadType.getParamKey() );
      out.write('"');
      out.write(' ');
      out.print( reloadType.getParamKey().equals(request.getParameter("type")) ? "selected" : "" );
      out.write('>');
      out.print( reloadType.getParamKey() );
      out.write("</option>\n    ");
 } 
      out.write("</select>\n    ");
 for (ReloadType reloadType : ReloadType.values()) { 
      out.write("<div class=\"cache-container\" id=\"");
      out.print( reloadType.getParamKey() );
      out.write("_panel\" style=\"display: none;\">\n      <div class=\"cache-title\">\n        Reload Cache\n      </div>\n      <hr>\n      <div class=\"cache-body\">\n        <form action=\"/internal/reloadcache\" method=\"POST\" onsubmit=\"return verifySubmit(this, '");
      out.print( reloadType.getParamKey() );
      out.write("');\">\n          <b>Reset by id</b> (use \"all\" to reset all):<br>\n          <textarea name=\"ids\" style=\"width: 500px; height: 100px;\"></textarea><br>\n          <input type=\"submit\" value=\"RESET\"/> <span style=\"color:red;\" id=\"");
      out.print( reloadType.getParamKey() );
      out.write("_error\"></span>\n          <input type=\"hidden\" name=\"type\" value=\"");
      out.print( reloadType.getParamKey() );
      out.write("\"/>\n        </form>\n      </div>\n      <br>\n      ");
 if (request.getAttribute(reloadType.getPassthroughTimestampKey()) != null) { 
      out.write("<b>Last reset: </b> ");
      out.print( request.getAttribute(reloadType.getPassthroughTimestampKey()) );
      out.write(" by ");
      out.print( request.getAttribute(reloadType.getPassthroughAgentKey()) );
 } else if (request.getParameter("type") != null) { 
      out.write("<b>Last reset: </b> <i>");
      out.print( request.getParameter("type") );
      out.write("</i> ");
      out.print( request.getParameter("ids") );
 } else { 
      out.write("<b>Last reset: </b> n/a\n      ");
 } 
      out.write("</div>\n    ");
 } 
      out.write("</body>\n</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
