var Table = function(opt_disableOrdering) {
  this.headings = [];
  this.rows = [];
  this.orderIndex = -1;
  this.disableOrdering = opt_disableOrdering;
};

Table.tables = {};

Table.comparator = function(a, b) {
  var aVal = a.getItem(Table.orderIndex).getValue();
  var bVal = b.getItem(Table.orderIndex).getValue();
  var isA = 0;
  if (isNaN(aVal)) {
    if (isNaN(bVal)) {
      isA = aVal.toLowerCase() > bVal.toLowerCase();
    } else {
      isB = 1;
    }
  } else if (isNaN(bVal)) {
    isA = 1;
  } else {
    isA = aVal > bVal;
  }
  console.log(aVal + ' ' + bVal + ' ' + isA);
  return isA && !Table.orderAsc ? -1 : 1;
};

Table.getLink = function(view, param) {
  return view + '/' + param;
};

Table.prototype.addRow = function(tableRow) {
  this.rows.push(tableRow);
};

Table.prototype.getRows = function() {
  return this.rows;
};

Table.prototype.setHeadings = function(headings) {
  this.headings = headings;
};

Table.prototype.getHeadings = function() {
  return this.headings;
};

Table.prototype.order = function(columnIndex) {
  this.orderAsc = this.orderIndex != columnIndex || !this.orderAsc;
  this.orderIndex = columnIndex;
  // TODO(larrystinson) hack.
  Table.orderAsc = this.orderAsc;
  Table.orderIndex = this.orderIndex;
  this.rows = this.rows.sort(Table.comparator);
};

Table.prototype.getOrderIndex = function() {
  return this.orderIndex;
};

Table.prototype.getOrderAsc = function() {
  return this.orderAsc;
};

var TableRow = function() {
  this.items = [];
};

TableRow.prototype.addItem = function(tableItem) {
  this.items.push(tableItem);
};

TableRow.prototype.getItems = function() {
  return this.items;
};

TableRow.prototype.getItem = function(index) {
  return this.items[index];
};

var TableItem = function(value, opt_displayValue, opt_link, opt_is_path) {
  this.value = value;
  this.displayValue = opt_displayValue ? opt_displayValue : value;
  this.link = opt_link;
  this.isPath = opt_is_path;
};

TableItem.prototype.getValue = function() {
  return this.value;
};

TableItem.prototype.getDisplayValue = function() {
  return this.displayValue;
};

TableItem.prototype.getLink = function() {
  if (this.link) {
    return (this.isPath ? '' : '#') + this.link;
  }
};

Table.drawTable = function(table, tableId, tableElementId) {
  var html = '<table class="bordered">';
  var headings = table.getHeadings();

  html += '<tr>';
  for (var i = 0; i < headings.length; i++) {
    if (table.disableOrdering) {
      html += '<th>';
    } else {
      html += '<th onclick="Table.orderTable(\'' + tableId + '\',\'' + tableElementId + '\',' + i + ');">';
    }
    html += headings[i];
    if (i == table.getOrderIndex()) {
      html += ' ' + (table.getOrderAsc() ? '&#9650;' : '&#9660;');
    }
    html += '</th>';
  }
  html += '</tr>';

  var rows = table.getRows();
  for (var i = 0; i < rows.length; i++) {
    var row = rows[i];
    var items = row.getItems();
    html += '<tr>';
    for (var j = 0; j < items.length; j++) {
      var item = items[j];
      if (item.getLink()) {
        html += '<td><a href="' + item.getLink() + '">' + item.getDisplayValue() + '</a></td>';
      } else {
        html += '<td>' + item.getDisplayValue() + '</td>';
      }
    }
    html += '</tr>';
  }
  html += '</table>';
  document.getElementById(tableElementId).innerHTML = html;
  Table.tables[tableId] = table;
};

Table.orderTable = function (tableId, tableElementId, columnIndex) {
  var table = Table.tables[tableId];
  table.order(columnIndex);
  Table.drawTable(table, tableId, tableElementId);
};
