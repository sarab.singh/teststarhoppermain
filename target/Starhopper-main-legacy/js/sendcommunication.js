// Global timer
var timer = null;

// A wrapper function for setTimeout(), except it will first clear a global timer.
function clearAndSetTimeout(callback, delay) {
  if (timer !== null) {
    clearTimeout(timer);
  }
  timer = setTimeout(callback, delay);
}

// Hides any open error alerts
function hideErrorAlert() {
  $("#error-box").stop().fadeOut(200)
}

// Hides any open success alerts
function hideSuccessAlert() {
  $("#success-box").stop().fadeOut(200)
}

// Shows an error alert with the given message.
function showErrorAlert(msg){
  $('#error-text').text(msg);
  $("#error-box").stop().slideDown(200);
}

// Shows an alert signifying a success, which will automatically close.
function showSuccessAlert() {
  $("#success-box").stop().slideDown(200);
  clearAndSetTimeout(hideSuccessAlert, 2000);
}

// Intercept the "submit" event from the form, and handle the request manually.
$('#form').on('submit', (event) => {
    // Prevent the form from submitting
    event.preventDefault();

    // Clear any open alerts
    hideErrorAlert();
    hideSuccessAlert();

    // Make a POST request with the form data, listen for the response.
    fetch(event.target.action, {
        method: 'POST',
        body: new URLSearchParams(new FormData(event.target))
    }).then((resp) => {
      if (resp.status !== 200) {
          showErrorAlert('Status Code: ' + resp.status);
          return;
      }

      // An error code of "OK" is shown iff there are no errors.
      resp.json().then(function(data) {
          if (data.errorCode === 'OK') {
            showSuccessAlert();
            $('#form').trigger('reset');
          }
          else {
            showErrorAlert(data.errorMessage);
          }
      });
      return;
    }).catch((error) => {
      showErrorAlert('Fetch encountered a problem: ', error);
    });
});