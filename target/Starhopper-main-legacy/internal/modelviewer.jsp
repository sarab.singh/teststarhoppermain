<%@ page import="co.ritual.internal.modelviewer.ModelViewer" %>
<%
  ModelViewer modelViewer = (ModelViewer) request.getAttribute(ModelViewer.PARAM_MODEL_VIEWER);
%>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>JQuery JSONView</title>
  <link rel="stylesheet" href="/internal/responseviewer/jquery.jsonview.min.css" />
  <style>
    body {
      background-color: light-gray;
      font-family: monospace;
    }
  </style>
  <script type="text/javascript" src="/internal/responseviewer/jquery.min.js"></script>
  <script type="text/javascript" src="/internal/responseviewer/jquery.jsonview.min.js"></script>
  <script type="text/javascript">
    var json = <%= modelViewer.getJson() %>;
    $(function() {
      $("#json").JSONView(json);
      // $("#json").JSONView(json, { collapsed: true, nl2br: true, recursive_collapser: true });
      $('#toggle-btn').on('click', function() {
        $('#json').JSONView('toggle');
      });
    });
  </script>
</head>
<body>
  <h2>Message</h2>
  <div id="message"><%= modelViewer.getMessage() %></div>
  <h2>Data</h2>
  <button id="toggle-btn">Toggle</button>
  <div id="json"></div>
</body>
</html>