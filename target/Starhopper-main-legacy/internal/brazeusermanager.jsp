<html>
  <head>
    <title>Manage Braze User</title>
    <style>
      .client-payload {
        background: #aaa;
      }
      .v1-notifications {
        background: #aaa;
      }
      .notification-body {
        background: #eee;
      }
      .group-order {
        background: #aaa;
      }
      .cache-title {
        font-size: 22px;
        font-weight: bold;
        text-align: center;
      }
    </style>
  </head>
  <body>
      <hr>
      <div class="page-body">

        <form action="/internal/brazeusermanager" method="POST">
            <b>Modify or create user attributes in braze</b>
            <div class="communication-body">
                <b>User ID</b><br>
                <input type="text" placeholder="External ID to send to" name="external_id" style="width: 500px; height: 20px;"></input><br>

                <b>Placeholders</b><br>
                <input placeholder="key:value,key:value..." name="placeholders" type="text"></input><br>
                <br>

                <b>Environment</b><br>
                <input name="environment" value="CONSUMER" type="radio" checked="checked"></input>Consumer<br>
                <input name="environment" value="REFERRAL" type="radio"></input>Referral<br>
                <input name="environment" value="PARTNER" type="radio"></input>Partner<br>

                <br>

                <input type="submit" value="SEND"/> <span style="color:red;"></span>
            </div>
        </form>
      </div>
  </body>
</html>