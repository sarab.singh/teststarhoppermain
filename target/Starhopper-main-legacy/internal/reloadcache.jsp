<%@ page import="co.ritual.internal.reloadcache.ReloadType"%>

<html>
  <head>
    <title>Reload Cache</title>
    <style>
      .cache-container {
        background: #eee;
      }
      .cache-title {
        font-size: 22px;
        font-weight: bold;
        text-align: center;
      }
    </style>
    <script>
      function verifySubmit(target, key) {
        if (key == 'admin') {
          return true;
        }

        var targets = target.getElementsByTagName('textarea');
        var hasId = false;
        for (var i = 0; i < targets.length; i++) {
          if (targets[i].value) {
            hasId = true;
            break;
          }
        }
        document.getElementById(key + '_error').innerHTML = hasId ? '' : 'Please specify an id';
        return hasId;
      };
      function onSelectCache(target) {
        if (target.previous_value) {
          document.getElementById(target.previous_value + '_panel').style.display = 'none';
        }
        document.getElementById(target.value + '_panel').style.display = 'block';
        target.previous_value = target.value;
      }
    </script>
  </head>
  <body onload="onSelectCache(document.getElementById('caches'));">
    <select id="caches" onchange="onSelectCache(this);">
    <% for (ReloadType reloadType : ReloadType.values()) { %>
      <option value="<%= reloadType.getParamKey() %>" <%= reloadType.getParamKey().equals(request.getParameter("type")) ? "selected" : "" %>><%= reloadType.getParamKey() %></option>
    <% } %>
    </select>
    <% for (ReloadType reloadType : ReloadType.values()) { %>
    <div class="cache-container" id="<%= reloadType.getParamKey() %>_panel" style="display: none;">
      <div class="cache-title">
        Reload Cache
      </div>
      <hr>
      <div class="cache-body">
        <form action="/internal/reloadcache" method="POST" onsubmit="return verifySubmit(this, '<%= reloadType.getParamKey() %>');">
          <b>Reset by id</b> (use "all" to reset all):<br>
          <textarea name="ids" style="width: 500px; height: 100px;"></textarea><br>
          <input type="submit" value="RESET"/> <span style="color:red;" id="<%= reloadType.getParamKey() %>_error"></span>
          <input type="hidden" name="type" value="<%= reloadType.getParamKey() %>"/>
        </form>
      </div>
      <br>
      <% if (request.getAttribute(reloadType.getPassthroughTimestampKey()) != null) { %>
        <b>Last reset: </b> <%= request.getAttribute(reloadType.getPassthroughTimestampKey()) %> by <%= request.getAttribute(reloadType.getPassthroughAgentKey()) %>
      <% } else if (request.getParameter("type") != null) { %>
        <b>Last reset: </b> <i><%= request.getParameter("type") %></i> <%= request.getParameter("ids") %>
      <% } else { %>
        <b>Last reset: </b> n/a
      <% } %>
    </div>
    <% } %>
  </body>
</html>
