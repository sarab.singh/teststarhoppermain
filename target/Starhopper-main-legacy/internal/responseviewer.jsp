<%@ page import="co.ritual.internal.responseviewer.ResponseViewer" %>
<%
  ResponseViewer responseViewer = (ResponseViewer) request.getAttribute(ResponseViewer.PARAM_RESPONSE_VIEWER);
%>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>JQuery JSONView</title>
  <link rel="stylesheet" href="/internal/responseviewer/jquery.jsonview.min.css" />
  <script type="text/javascript" src="/internal/responseviewer/jquery.min.js"></script>
  <script type="text/javascript" src="/internal/responseviewer/jquery.jsonview.min.js"></script>
  <script type="text/javascript">
    var json = <%= responseViewer.getJson() %>;
    $(function() {
      $("#json").JSONView(json);
      // $("#json").JSONView(json, { collapsed: true, nl2br: true, recursive_collapser: true });
      $('#collapse-btn').on('click', function() {
        $('#json').JSONView('collapse');
      });
      $('#expand-btn').on('click', function() {
        $('#json').JSONView('expand');
      });
      $('#toggle-btn').on('click', function() {
        $('#json').JSONView('toggle');
      });
      $('#toggle-level1-btn').on('click', function() {
        $('#json').JSONView('toggle', 1);
      });
      $('#toggle-level2-btn').on('click', function() {
        $('#json').JSONView('toggle', 2);
      });
    });
  </script>
</head>
<body>
  <h2>Message</h2>
  <div id="message"><%= responseViewer.getMessage() %></div>
  <h2>Data</h2>
  <button id="collapse-btn">Collapse</button>
  <button id="expand-btn">Expand</button>
  <button id="toggle-btn">Toggle</button>
  <button id="toggle-level1-btn">Toggle level1</button>
  <button id="toggle-level2-btn">Toggle level2</button>
  <div id="json"></div>
</body>
</html>