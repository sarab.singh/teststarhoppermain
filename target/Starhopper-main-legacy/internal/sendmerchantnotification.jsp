<%@ page import="co.ritual.proto.MerchantNotifications.MerchantRequiredAction.MerchantRequiredActionType"%>
<html>
  <body>
    <div>
      <form action="/internal/sendmerchantnotification" method="POST">
        <b> Merchant id(s) </b>
        <input type="text" placeholder="id1, id2, id3" name="ids" style="width: 500px; height: 20px;"></input><br>
        <b>Merchant Required Action type </b>
        <select name="action_type">
          <% for (MerchantRequiredActionType type : MerchantRequiredActionType.values()) { %>
            <option value="<%= type.name() %>"><%= type.name() %></option>
          <% } %>
        </select><br>
        <b> Argument (e.g. order id) </b>
        <input type="text" placeholder="arg1" name="arg1" style="width: 500px; height: 20px;"></input><br>
        <br>
        <input type="submit" value="SEND"/> <span style="color:red;"></span>
    </div>
  </body>
</html>
