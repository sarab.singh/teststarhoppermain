<html>
  <head>
    <title>Send Message</title>
    <style>
      .client-payload {
        background: #aaa;
      }
      .v1-notifications {
        background: #aaa;
      }
      .notification-body {
        background: #eee;
      }
      .group-order {
        background: #aaa;
      }
      .cache-title {
        font-size: 22px;
        font-weight: bold;
        text-align: center;
      }
    </style>
  </head>
  <body>
      <hr>
      <div class="page-body">

        <form action="/internal/sendmessage" method="POST">
            <b>Sends a message using LoudTest as the default template to the specified user ID.</b>
            <div class="notification-body">
                <b>User ID</b><br>
                <input type="text" placeholder="User ID to send to" name="id" style="width: 500px; height: 20px;"></input><br>

                <b>Template name for Notification/Email or SMS body</b><br>
                <input placeholder="LoudTest" name="template_name" type="text"></input><br>
                <b>Notification ID (For Notification Only)</b><br>
                <input placeholder="TestID" name="nid" type="text"></input><br>
                <b>Locale</b><br>
                <input placeholder="en-US" name="locale_id" type="text"></input><br>
                <br>

                <b>Message Type</b><br>
                <input name="type" value="NOTIFICATION" type="radio" checked="checked"></input>Notification<br>
                <input name="type" value="EMAIL" type="radio"></input>Email<br>
                <input name="type" value="SMS" type="radio"></input>SMS<br>

                <br>

                <input type="submit" value="SEND"/> <span style="color:red;"></span>
            </div>
        </form>
      </div>
  </body>
</html>