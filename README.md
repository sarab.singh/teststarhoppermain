# Starhopper Analytics

This Jenkins pipeline involve the below steps

1) Validating the pre-flight check
2) Build and push image with Container Builder
3) Helm Charts for deployment in GKE cluster

The agent of Jenkins Pipeline used Kubernetes for creating Container template and Container builder.

The containers contain the image for gcloud, kubectl and Helm

1) Validating pre-flight check
- It will validate all the required tool for this pipeline

2) Build and push image with Container Builder
- Container Builder will build and push the images to GCR using gcloud
- imageRepo and imageTag has been defined as environment variable and the tag will increment with every build

3) Helm Chart for deployment
- Helm Chart is being used define, install and upgrade the kubernetes application

helmcharts/
  Chart.yaml: A YAML file containing brief information about the starhopper-analytics chart
  README.md: A human-readable README file about chart structure
  values.yaml: The default configuration values for the starhopper-analytics chart. It provide the default values for Deployment, service and ingress yaml file.
  templates/: A directory of templates that, when combined with values, will generate valid Kubernetes manifest files.
  templates/Deployment.yml: Contain the deployment info such as imageRepo, imageTag, labels, replicaCount and so on.
  template/service.yml: It create service for the existing deployment
  template/ingress.yml: Ingress is used to expose to outside world by creating load balancer.